// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU

#include "TorusServoWinch.h"
#include "TorusConfig.h"
#include "elapsedMillis.h"

#include "DebugCSPU.h"

void waitForEnter() {
  while (Serial.available() > 0) Serial.read();
  Serial.flush();
  while (Serial.available() == 0);
  Serial.read();
}

void progressBar(int duration /* in seconds */) {
  for(int i = 0; i < 10; i++) {
    Serial << ".";
    delay(duration*100);
  }
}

void setup() {
  DINIT(115200); Serial << ENDL;
  TorusServoWinch servo;
  Serial << "TORUS SERVO ASSEMBLY UTILITY" << ENDL << "----------------------------" << ENDL << ENDL;
  
  Serial << "1. Detach anything that is currently attached to the servo gears (eg. the rotative drum)." << ENDL;
  Serial << "   When done, press enter (once pressed, the motor will start moving)." << ENDL;
  waitForEnter();
  Serial << "   Motor is currently moving. Please wait for the next instruction." << ENDL;
  servo.begin();
  servo.set(ServoInitAndEndPositionType, ServoInitAndEndPosition+20);
  Serial << "   ";
  progressBar(10 /* it shouldn't take longer than 10 seconds */);
  Serial << ENDL;
  Serial << "   Servo movement complete." << ENDL << ENDL;
  
  Serial << "2. Secure the parachute thread in the rotative drum of the servo and put the drum back in place (attached to the gears) such that: " << ENDL;
  Serial << "     - the thread is not rolled around the drum by ANY amount (ie. it is FULLY extended)" << ENDL;
  Serial << "     - when the drum is put back in its position in the can, the thread length is 2 cm longer than it should be when the parachute is in the fully extended configuration." << ENDL << ENDL;
  
  Serial << "3. When you press enter, the motor will start moving." << ENDL;
  Serial << "   While it moves, apply slight pressure on the tread by pulling it to keep the thread extended and to prevent it from getting jammed up." << ENDL;
  waitForEnter();
  Serial << "   Motor is currently moving. Please keep the rope exended." << ENDL;
  servo.set(ServoInitAndEndPositionType, ServoInitAndEndPosition);
  Serial << "   ";
  progressBar(2 /* it shouldn't take longer than 2 seconds */);
  Serial << ENDL;
  Serial << "   Servo movement complete." << ENDL << ENDL;

  Serial << "----------------------------" << ENDL << "PROCEDURE COMPLETED";
}

void loop() {}
