/*
   WormScrewLatch.h
*/

#pragma once

#include "Arduino.h"
#include "Latch.h"
#include "HBridgeMotor.h"

/** @ingroup CSPU_Motors
    @brief Implementation of a Latch based on a worm screw and a motor power using a H-bridge.
    The class has been tested with:
    - Motor TO BE COMPLETED
    - H-Bridge L923D.
*/

class WormScrewLatch : public Latch, public HBridgeMotor {

  public :
    /** Method to call before any other method. Initializes whatever must be.
        @param pwmPin the PWM Pin number actives the H-Bridge.
        @param forwardPin the Forward Pin is used to turn forward.
        @param reversePin the Reverse Pin is used to turn backwards.
        @param power the Power is the velocity of the motor (between 0 and 255).
        @param delayLock the Delay Lock is used to lock.
        @param delayUnlock the Delay Unlock is used to unlock.
        @return true if motor initialisation succeeded
    */
    bool begin(uint8_t pwmPin, uint8_t forwardPin, uint8_t reversePin, uint8_t power, uint32_t delayLock, uint32_t delayUnlock);
    virtual void unlock() override; /**< Method to unlock */
    virtual void lock() override; /**< Method to lock */

  private :
    uint16_t screwLatchPower; /**< Motor velocity (between 0 and 255) defined in GMiniConfig.h */
    uint32_t screwLatchDelayUnlock; /**< Delay in milliseconds to unlock ; precise delay to be defined in GMiniConfig.h */
    uint32_t screwLatchDelayLock; /**< Delay in milliseconds to lock ; precise delay to be defined in GMiniConfig.h */

};
