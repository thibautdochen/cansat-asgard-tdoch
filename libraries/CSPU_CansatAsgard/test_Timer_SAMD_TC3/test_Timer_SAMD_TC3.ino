/*
    test_SAMD_TC3
    See: http://forum.sodaq.com/t/autonomo-internal-interupts-timers/51/7 

    This demonstrates how to install a 1024 Hz time interrupt. 
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef __SAMD21G18A__
#error "This sketch is only for SAMD boards"
#endif

#define DEBUG_CSPU
#include "DebugCSPU.h"

uint32_t counter= 0;
unsigned long refMillis;

volatile bool TC_flag_CC1 = false;

void configureTC3(uint16_t match)
{
  uint8_t GCLK_SRC = 4;                                               // clock 4, (0,1,3 are used)
  GCLK->GENDIV.reg = GCLK_GENDIV_ID(GCLK_SRC) | GCLK_GENDIV_DIV(1);   // source | prescaler
  //enable clock mod | select the oscilator source | select clock | prescalar | keep running in standby | 
  GCLK->GENCTRL.reg = GCLK_GENCTRL_GENEN | GCLK_GENCTRL_SRC_XOSC32K | GCLK_GENCTRL_ID(GCLK_SRC) | GCLK_GENCTRL_RUNSTDBY | GCLK_GENCTRL_DIVSEL;
  while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY);
  PM->APBCMASK.reg |= PM_APBCMASK_TC3;                                // Turn the power to the TC3 module on
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN |  GCLK_CLKCTRL_GEN(GCLK_SRC) |  GCLK_CLKCTRL_ID(GCM_TCC2_TC3); // tc3 source to clock | generator source | select clock
  while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY);
  TC3->COUNT16.CTRLA.reg &= ~TC_CTRLA_ENABLE;                         //disable TC3
  while (TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);
  TC3->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 | TC_CTRLA_RUNSTDBY | TC_CTRLA_PRESCALER_DIV1 ; // count xbit | standby | prescalar_div1 | 
  while (TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY); //wait
  TC3->COUNT16.CC[1].reg = match;                                     // Set the compare channel 1 value
  while (TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);//wait
  TC3->COUNT16.INTENSET.reg = TC_INTENSET_MC1;                        // enable interrupt
  while (TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);//wait
  TC3->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;                          // enable interrupt
  while (TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);//wait
  NVIC_EnableIRQ(TC3_IRQn);                                           // Enable the TC3 interrupt vector
  NVIC_SetPriority(TC3_IRQn, 0x00);                                   // Set the priority to max
}
void TC3_Handler()
{
  if (TC3->COUNT16.INTFLAG.reg & TC_INTFLAG_MC1) 
  {
    TC3->COUNT16.COUNT.reg = 0;                                       // Reset the counter to repeat  
    TC3->COUNT16.INTFLAG.reg |= TC_INTFLAG_MC1;                       // Reset MC1 interrupt flag
    TC_flag_CC1 = true; //Set compare match flag
    counter++;  
  }
}

void setup()
{
  DINIT(115200);
  configureTC3(4);
  Serial << "Configuration Complete" << ENDL;
  refMillis=millis();
}

void loop() {
  unsigned long ts=millis();
  float elapsed= ts-refMillis;
  Serial << ts << ": elapsed= " << elapsed << ": counter=" << counter << " freq=" 
         << ((float) counter) / elapsed * 1000.0 << " Hz" << ENDL;
  delay(1000);
}
