/*
 * Latch.h
 */

#pragma once
#include "CansatConfig.h"

/** @ingroup CSPU_CansatAsgard
    @brief This an abstract class for any latch mechanism. It supports 2 mandatory states
    	   (locked and unlocked) and an option neutral one (in which the latch is
    	   "loose" and can be locked or unlocked by an external actuator).
    Actual latches are implemented base on servos, motors etc. which a interface by
    specialized subclasses of Latch.
 */
class Latch {
  public :
	Latch() : theStatus(LatchStatus::Undefined) {};
    /** Enum values representing different servo status*/
    enum class LatchStatus {
      Neutral = 0,
      Locked = 1,
      Unlocked = 2,
      Undefined = 3
    };
    virtual ~Latch() {};

    /** Set the latch in "unlocked" position */
    virtual void unlock()=0;

    /** Set the latch in "locked" position */
    virtual void lock()=0;

    /** Set the latch in "neutral" position (the base class implementation just
     *  issues an error message. Subclasses supporting the neutral position should
     *  override the method, implement it and update the Latch::status variable
     *  accordingly*/
    virtual void neutral(){
    	DPRINTSLN(DBG_DIAGNOSTIC,"Latch::neutral called. Implement in subclass");
    };

    inline LatchStatus getStatus() { return theStatus; }; /**< The LatchStatus object*/

  protected :
    LatchStatus theStatus; /**< The enum class object*/
};



