#include "CO2_Client.h"
#include "HardwareScanner.h"

CO2_Client CO2(A1);
IsaTwoRecord record;
HardwareScanner hwscanner;

void setup() {
  hwscanner.init();
  CO2.begin(hwscanner.getDefaultReferenceVoltage(), hwscanner.getNumADC_Steps());
  Serial.begin(115200);
}

void loop() {
  CO2.readData(record);
  float concentration = ((record.co2_voltage - 0.4) * 5000) / 1.60;
  Serial.println(concentration);
  Serial.println(record.co2_voltage);
  delay(100);
}
