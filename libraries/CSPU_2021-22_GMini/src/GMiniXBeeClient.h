/*
 * GMiniXBeeClient.h
 */

#pragma once
#include "CansatConfig.h" // For the definition of RF_ACTIVATE_API_MODE!
#ifdef RF_ACTIVATE_API_MODE

#include "CansatXBeeClient.h"
#include "GMiniConfig.h"

#define DBG_DIAGNOSTIC 1

/** @ingroup GMiniCSPU
 *  @brief A CansatXBeeClient handling the RF Communication specific
 *  to the G-Mini project (the specificity is only in the definition of the
 *  system components and the configuration of the various corresponding XBee
 *  modules. Data records and string transmission is inherited without any change.
 */
class GMiniXBeeClient: public CansatXBeeClient {
public:
	/**
	 *  An enum identifying the various components of the G-Mini system.
	 */
	enum class GMiniComponent {
		Undefined=CansatCanID::Undefined,
		MainCan=CansatCanID::MainCan,
		SubCan1=CansatCanID::SubCan1,
		SubCan2=CansatCanID::SubCan2,
		SubCan3=CansatCanID::SubCan3,
		SubCan4=CansatCanID::SubCan4,
		RF_Transceiver=(uint8_t) CansatCanID::SubCan4 + 1
	};
	GMiniXBeeClient(uint32_t defaultDestinationSH, uint32_t defaultDestinationSL)
		: CansatXBeeClient(defaultDestinationSH, defaultDestinationSL) {};
	virtual ~GMiniXBeeClient() {};

	/** Identify by which system component this XBee module is used.
	 *  @param sh The upper part of the 64 bits address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @param sl The lower part of the 64 bit address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 * @return The system component, or GMiniComponent::Undefined if
	 * 		   the module does not appear in GMiniConfig.h*/
	GMiniComponent getXBeeSystemComponent(const uint32_t &sh=0, const uint32_t &sl=0);

	/** Obtain a human-readable label for a particular RF strategy.
	 *  @param strategy The strategy to label.
	 *  @return The corresponding label. */
	static const char* getLabel(GMiniRF_Strategy strategy);

	/** Obtain a human-readable label for a particular system component.
	 *  @param component The component to label.
	 *  @return The corresponding label. */
	static const char* getLabel(GMiniComponent component);

	virtual bool printConfigurationSummary(Stream &stream,
			const uint32_t & sh=0, const uint32_t& sl=0) override;
	virtual bool checkCommonModuleConfiguration(bool correctConfiguration,
			bool &configurationChanged,
			const uint32_t& sh=0, const uint32_t& sl=0 ) override;


protected:
	/** Utility method to define the role allocated to the XBee module (from
	 *  GMiniConfig.h, check the configuration and possibly update it (permanently).
	 *  The RF communication strategy as defined in GMiniConfig.h is taken into
	 *  account.
	 *  @param correctConfiguration If true, and the XBee is referenced in GMiniConfig.h,
	 *         						update configuration if it is not correct.
	 *  @param configurationChanged (out) True if any change was applied to the
	 *  						  	configuration, false otherwise.
	 *  @param sh The upper part of the 64 bits address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @param sl The lower part of the 64 bit address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @return true if the module was identified and the configuration is ok.
	 *  		true if the module was identified and the configuration was not ok,
	 *  		     correctConfiguration=true and configuration was successfully updated
	 *  		false otherwise.
	 */
	virtual bool doCheckXBeeModuleConfiguration(bool correctConfiguration,
									  bool &configurationChanged,
									  const uint32_t& sh=0,const uint32_t& sl=0) override;

	/** Check the configuration of the XBee module, assuming it is used in
	 *  the main can and possibly update it (permanently).
	 *  The RF communication strategy as defined in GMiniConfig.h is taken into
	 *  account.
	 *  @param correctConfiguration If true, and the XBee is referenced in GMiniConfig.h,
	 *         						update configuration if it is not correct.
	 *  @param configurationChanged (out) True if any change was applied to the
	 *  						  	configuration, false otherwise.
	 *  @param sh The upper part of the 64 bits address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @param sl The lower part of the 64 bit address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @return true if the configuration is ok.
	 *  		true if the configuration was not ok, correctConfiguration=true
	 *  		 	 and configuration was successfully updated
	 *  		false otherwise.
	 */
	virtual bool checkCanModuleConfiguration(bool correctConfiguration,
	  bool &configurationChanged,
	  const uint32_t& sh=0,const uint32_t& sl=0) override;

	/** Check the configuration of the XBee module, assuming it is used in
	 *  a subcan and possibly update it (permanently).
	 *  The RF communication strategy as defined in GMiniConfig.h is taken into
	 *  account.
	 *  @param subCanID The component ID of the subcan, as defined in CansatInterface.h
	 *  @param correctConfiguration If true, and the XBee is referenced in GMiniConfig.h,
	 *         						update configuration if it is not correct.
	 *  @param configurationChanged (out) True if any change was applied to the
	 *  						  	configuration, false otherwise.
	 *  @param sh The upper part of the 64 bits address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @param sl The lower part of the 64 bit address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @return true if the configuration is ok.
	 *  		true if the configuration was not ok, correctConfiguration=true
	 *  		 	 and configuration was successfully updated
	 *  		false otherwise.
	 */
	bool checkSubCanModuleConfiguration(CansatCanID subCanID,
			  bool correctConfiguration,
			  bool &configurationChanged,
			  const uint32_t& sh=0,const uint32_t& sl=0);
	/** Check the configuration of the XBee module, assuming it is used in
	 *  the RF_Transceiver and possibly update it (permanently).
	 *  The RF communication strategy as defined in GMiniConfig.h is taken into
	 *  account.
	 *  @param correctConfiguration If true, and the XBee is referenced in GMiniConfig.h,
	 *         						update configuration if it is not correct.
	 *  @param configurationChanged (out) True if any change was applied to the
	 *  						  	configuration, false otherwise.
	 *  @param sh The upper part of the 64 bits address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @param sl The lower part of the 64 bit address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @return true if the configuration is ok.
	 *  		true if the configuration was not ok, correctConfiguration=true
	 *  		 	 and configuration was successfully updated
	 *  		false otherwise.
	 */
	virtual bool checkGroundModuleConfiguration(bool correctConfiguration,
			  bool &configurationChanged,
			  const uint32_t& sh=0,const uint32_t& sl=0) override;

};
#endif
