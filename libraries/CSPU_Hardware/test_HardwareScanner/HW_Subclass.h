/*
 * A subclass for testing the HardwareScanner and CansatHW_Scanner
 */

#pragma once
#include "CansatHW_Scanner.h"

class HW_Subclass: public CansatHW_Scanner {
public:
	HW_Subclass();

	virtual byte getNumExternalEEPROM() const { return 3;}
	virtual byte getExternalEEPROM_Address(const byte EEPROM_Number) const ;
	virtual unsigned int getExternalEEPROM_LastAddress(const byte EEPROM_Number) const ;

	virtual byte getLED_PinNbr(LED_Type type);
};
