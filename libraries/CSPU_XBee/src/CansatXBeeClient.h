/*
   CansatXbeeClient.h
*/

#pragma once

#include "CansatConfig.h" // for RF_ACTIVATE_API_MODE
#include "XBeeClient.h"
#include "CansatInterface.h" // Required anyway by the macro's.

#ifdef RF_ACTIVATE_API_MODE
// This symbol is defined in CansatConfig.h. Note that XBeeClient.h should be included anyway:
// It contains some macro's useful in Transparent mode.

#include "CansatRecord.h"

/**
 * @ingroup CSPU_XBee
 * @brief Subclass of XBeeClient adding the capability to transport CansatRecords and
 *        string of predefined types easily (types are defined by enum CansatFrameType
 *        in CansatInterface.h). It also implements the automated configuration of
 *        XBee modules
 *
 * @par Emission of a CansatRecord
 * 		Just use the send() method.
 * @par Emission of a string message
 * 			 @code
 *        	 openStringMsg(CansatFrameType::xxxxxx);
 *        	 myCansatXBeeClient << "my string"
 *        	 closeStringMsg();
 * 			 @endcode
 * @par Reception
 *			 loop on:
 *           @code
 *           MySubclassOfCansatRecord myRecord;
 *           char	myString[maxStringLength];
 *           CansatFrameType theType;
 *           bool gotRecord;
 *           if (receive(myRecord, myString, theType, gotRecord)) {
 *           	if (gotRecord) {
 *           		 		... handle the record...
 *           	else {
 *           	   ... handle myString according to theType.
 *           	}
 *           }
 *           @endcode
 * @par Configuration (access, check, update...) of XBee modules
 * 		Use the following methods:
 * 		- printConfigurationSummary() to display the XBee module configuration
 * 		  relevant to Cansat.
 * 		- checkXBeeModuleConfiguration() to check whether an XBee module is properly
 * 		  configuration for Cansat, and possibly fix the configuration.
 * 		- getXBeeSystemComponent() to define the system component associated with
 * 		  an XBee module, according to CansatConfig.h.
 * 		- getXBeeModuleID() to define the ID allocated by CansatConfig.h to a particular
 * 		  XBee module.
 * @par Configuration parameters:
 * 	 - Protected constant CansatXBeeClient::displayOutgoingMsgOnSerial and
 * 	   displayIncomingMsgOnSerial control the echo of messages on Serial
 * 	   (should be false in operation).
 */
class CansatXBeeClient : public XBeeClient {
  public:
	/** An enum identifying the various components of the G-Mini system. */
	enum class CansatComponent {
		Undefined= (uint8_t) CansatCanID::Undefined,
		Can=(uint8_t) CansatCanID::MainCan,
		RF_Transceiver=(uint8_t) CansatCanID::MainCan + 1
	};
	CansatXBeeClient(uint32_t defaultDestinationSH, uint32_t defaultDestinationSL):
		XBeeClient(defaultDestinationSH, defaultDestinationSL) {};

    virtual ~CansatXBeeClient() {};
	/** @name Transmitting data
	 *  Those functions are used to transmit data through a pair of XBee module.
	 */
	///@{
    /**Send a data record (after begin() has been called).
       @param record, The payload to transfer
       @param timeOutCheckResponse The maximum delay to wait for a reception
        				acknowledgment by the destination XBee (msec). If 0,
        				the reception ack will not be checked.
       @param destSH The destination address (high word)
       @param destSL The destination address (low word).  If destSH=destSL=0,
        	         the default address provided in the constructor is used.

       @return True if transfer is successful, false otherwise
    */
    virtual bool send(	const CansatRecord& record,
    					int timeOutCheckResponse = 0,
						uint32_t destSH=0, uint32_t destSL=0);

    /** Check whether data is received (record or string). This should be used in the
     *  receiver's main loop (after begin() has been called).
     *  @param record The record to be fill with the data (when a record is received)
     *  @param string The buffer to be filled with the null-terminated string (when a
     *  			string is received.
	 * 			 	It is allocated by the caller, must allow for the longest possible
	 * 			 	string transported (which is MaxStringLength+1 byte).
	 * @param stringType The frame type associated with a received string.
	 * @param seqNbr The optional sequence number associated with the string.
	 * @param gotRecord This is set to true when a record is received, false when a
	 *     		    string is received.
	 * @return True if anything received, false otherwise
     */
    virtual bool receive(CansatRecord& record, char* string, CansatFrameType &stringType,
    					uint8_t& seqNbr, bool& gotRecord);

    /** Check whether a string is received (record are discarded). This should be used in the
     *  receiver's main loop (after begin() has been called) when waiting for strings only.
     *  @param string The buffer to be filled with the null-terminated string (when a
     *  			string is received.
	 * 			 	It is allocated by the caller, must allow for the longest possible
	 * 			 	string transported (which is MaxStringLength+1 byte).
	 * @param stringType The frame type associated with a received string.
 	 * @param seqNbr The optional sequence number associated with the string.
	 * @return True if a valid string was received, false otherwise
     */
    virtual bool receive(char* string, CansatFrameType &stringType, uint8_t& seqNbr	);

    /** Begin a new string message of the provided type. This should be called
     *  whenever starting a new string message (before using the streaming operator
     *  and closing the message with closeStringMessage).
     *  @param recordType The record type set in the frame's first byte.
     *  @param seqNbr A number that can be used to define a sequence in the strings
     *  	   to allow the receiver to check that no message was lost.
    */
    virtual void openStringMessage(CansatFrameType frameType = CansatFrameType::StatusMsg, uint8_t seqNbr=0xFF);
    ///@}

    /** Extract data from the payload to fill a CansatRecord.
       @param record The record to fill with the data
       @param data Payload pointer
       @param dataSize Payload size
       @pre The payload is expected to be exactly a byte with value
      		CansatFrameType::DataRecord, an unused byte and
            a CansatRecord.
       @return True if operation was successful, false otherwise.
    */
    bool getDataRecord(CansatRecord& record, const uint8_t* data, uint8_t dataSize) const;
    
    /** Extract data from the payload to fill a string buffer.
	* @param data The buffer to fill with the null-terminated string.
	* 			 allocated by the caller, must be at least dataSize bytes.
	* @param stringType The frame type found in the payload.
	* @param seqNbr The optional sequence number associated with the string.
	* @param data Payload pointer
	* @param dataSize Payload size
	* @param silentIfError If true, no error message is provided when the
	* 	   provided buffer does not contain a valid string.
	* @pre The payload is expected to be exactly a byte with value
	* 		CansatFrameType::StatusMsg, CmdRequest, CmdResponse or StringPart,
	* 		followed by an unused byte and a null-terminated string.
	* @return True if operation was successful, false otherwise.
	*/
   	bool getString(char* string, CansatFrameType &stringType, uint8_t& seqNbr,
   			const uint8_t* data,uint8_t dataSize, bool silentIfError=false) const;

   	/** Print the content of the payload when containing a string
   	 *  (for debugging only).
   	 *  @param data Payload pointer
   	 *  @param dataSize Payload size
   	 */
    virtual void printString(uint8_t* data, uint8_t dataSize) const override;

    virtual bool printConfigurationSummary(Stream &stream,
    		const uint32_t & sh=0, const uint32_t& sl=0) override;

	/** Check the configuration common to any XBee module, possibly update it,
	 *  but this method does not trigger a permanent update.
	 *  The RF communication strategy as defined in CansatConfig.h is taken into
	 *  account.
	 *  @param correctConfiguration If true, and the XBee is referenced in CansatConfig.h,
	 *         						update configuration if it is not correct.
	 *  @param configurationChanged (out) True if any change was applied to the
	 *  						  	configuration, false otherwise.
	 *  @param sh	The serial number high of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
	 *  @return true if the configuration is ok.
	 *  		true if the configuration was not ok, correctConfiguration=true
	 *  		 	 and configuration was successfully updated
	 *  		false otherwise.
	 */
	virtual bool checkCommonModuleConfiguration(bool correctConfiguration,
			  bool &configurationChanged,
			  const uint32_t& sh=0, const uint32_t& sl=0 );

	/** Identify by which system component this XBee module is used.
	 *  @param sh The upper part of the 64 bits address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @param sl The lower part of the 64 bit address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 * @return The system component, or CansatComponent::Undefined if
	 * 		   the module does not appear in CansatConfig.h */
    CansatXBeeClient::CansatComponent getXBeeSystemComponent(
    			 const uint32_t &sh=0, const uint32_t &sl=0);

	/** Obtain a human-readable label for a particular system component.
	 *  @param component The component to label.
	 *  @return The corresponding label. */
	static const char* getLabel(CansatComponent component);

	/** Obtain the ID of an XBee module, from its 64-bits address.
	 *  Compare the module address with the various XBeeAddressSH_nn and
	 *  XBeeAddressSL_nn constants defined in CansatConfig.h, and return the
	 *  corresponding Id (or 0 if not found).
	 *  @param sh The upper part of the 64 bits address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @param sl The lower part of the 64 bit address (if sl=dl=0, the address
	 *  		  of the local module is retrieved)
	 *  @return The module ID (0 if not found).
	 */
	 uint8_t getXBeeModuleID(const uint32_t &sh=0, const uint32_t &sl=0);

  protected:
	 /** Utility method to define the role allocated to the XBee module (from
	  *  CansatConfig.h, check the configuration and possibly update it (permanently).
	  *  @param correctConfiguration If true, and the XBee is referenced in CansatConfig.h,
	  *         						update configuration if it is not correct.
	  *  @param sh	The serial number high of the Xbee module concerned (sh=sl=0 is the local
	  *  			module).
	  *  @param sl	The serial number low of the Xbee module concerned (sh=sl=0 is the local
	  *  			module).
	  *  @return true if the module was identified and the configuration is ok.
	  *  		true if the module was identified and the configuration was not ok,
	  *  		     correctConfiguration=true and configuration was successfully updated
	  *  		false otherwise.
	  */
	 virtual bool doCheckXBeeModuleConfiguration(bool correctConfiguration,
										  bool &configurationChanged,
										  const uint32_t& sh=0, const uint32_t& sl=0) override;


	 /* Replace the first 2 characters of the name with the XBee module ID according
	  * to configuration defined CansatConfig.h (and obtained from getXBeeModuleID())
	 *  @param sh	The serial number high of the Xbee module concerned (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module concerned (sh=sl=0 is the local
	 *  			module).
	  * @param name The name to update. Must be at least 5 characters long.
	  */
	 void updateModuleID_Prefix(char* name,const uint32_t& sh=0, const uint32_t& sl=0);

	 /** Check the configuration of the XBee module, assuming it is used in
	 *  the main can and possibly update it (permanently).
	 *  @param correctConfiguration If true, and the XBee is referenced in CansatConfig.h,
	 *         						update configuration if it is not correct.
	 *  @param configurationChanged (out) True if any change was applied to the
	 *  						  	configuration, false otherwise.
	 *  @param sh	The serial number high of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
	 *  @return true if the configuration is ok.
	 *  		true if the configuration was not ok, correctConfiguration=true
	 *  		 	 and configuration was successfully updated
	 *  		false otherwise.
	 */
	virtual	bool checkCanModuleConfiguration(bool correctConfiguration,
			bool &configurationChanged,const uint32_t& sh=0, const uint32_t& sl=0 );

	/** Check the configuration of the XBee module, assuming it is used in
	 *  the RF_Transceiver and possibly update it (permanently).
	 *  @param correctConfiguration If true, and the XBee is referenced in CansatConfig.h,
	 *         						update configuration if it is not correct.
	 *  @param configurationChanged (out) True if any change was applied to the
	 *  						  	configuration, false otherwise.
	 *  @param sh	The serial number high of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
	 *  @return true if the configuration is ok.
	 *  		true if the configuration was not ok, correctConfiguration=true
	 *  		 	 and configuration was successfully updated
	 *  		false otherwise.
	 */
	virtual	bool checkGroundModuleConfiguration(bool correctConfiguration,
			bool &configurationChanged,const uint32_t& sh=0, const uint32_t& sl=0 );
    static constexpr bool DisplayOutgoingMsgOnSerial = false; /**< Activate for debugging only */
    static constexpr bool DisplayIncomingMsgOnSerial = false; /**< Activate for debugging only */
};

// The following macro's allow for sending strings with the same instructions
// in API and in transparent mode.
#  define GET_RF_STREAM(CansatHardwareScanner) CansatHardwareScanner.getRF_XBee();
#  define RF_OPEN_STRING(object) object->openStringMessage(CansatFrameType::StatusMsg);
#  define RF_OPEN_STRING_PART(object, seqNbr) object->openStringMessage(CansatFrameType::StringPart, seqNbr);
#  define RF_OPEN_CMD_REQUEST(object) object->openStringMessage(CansatFrameType::CmdRequest);
#  define RF_OPEN_CMD_RESPONSE(object) object->openStringMessage(CansatFrameType::CmdResponse);
#  define RF_CLOSE_STRING(object) object->closeStringMessage(0);
#  define RF_CLOSE_STRING_PART(object) object->closeStringMessage(0);
#  define RF_CLOSE_CMD_REQUEST(object) object->closeStringMessage(0);
#  define RF_CLOSE_CMD_RESPONSE(object) object->closeStringMessage(0);
#  define RF_CLOSE_STRING_WITH_ACK(object, delay) object->closeStringMessage(delay);
#  define RF_CLOSE_STRING_PART_WITH_ACK(object, delay) object->closeStringMessage(delay);
#  define RF_CLOSE_CMD_REQUEST_WITH_ACK(object, delay) object->closeStringMessage(delay);
#  define RF_CLOSE_CMD_RESPONSE_WITH_ACK(object, delay) object->closeStringMessage(delay);
#else
#  define GET_RF_STREAM(CansatHardwareScanner) CansatHardwareScanner.getRF_SerialObject();
#  define RF_OPEN_STRING(object) *object << (int) CansatFrameType::StatusMsg <<",";
#  define RF_OPEN_STRING_PART(object, seqNbr) *object << (int) CansatFrameType::StringPart <<",";
#  define RF_OPEN_CMD_REQUEST(object) *object << (int) CansatFrameType::CmdRequest <<",";
#  define RF_OPEN_CMD_RESPONSE(object) *object << (int) CansatFrameType::CmdResponse <<",";
#  define RF_CLOSE_CMD_REQUEST(object) *object << ENDL;
#  define RF_CLOSE_CMD_RESPONSE(object)  *object << ENDL;
#  define RF_CLOSE_STRING(object)  *object << ENDL;
#  define RF_CLOSE_STRING_PART(object)  *object << ENDL;
#  define RF_CLOSE_STRING_WITH_ACK(object, delay) *object << ENDL;
#  define RF_CLOSE_STRING_PART_WITH_ACK(object, delay) *object << ENDL;
#  define RF_CLOSE_CMD_REQUEST_WITH_ACK(object, delay) *object << ENDL;
#  define RF_CLOSE_CMD_RESPONSE_WITH_ACK(object, delay) *object << ENDL;

#endif
