// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "EEPROM_BankWithTools.h"
#include "EEPROM_Bank.h"
// #include "EEPROM_BankWriter.h"
// #include "HardwareScanner.h"
#define DEBUG_CSPU
///#define USE_ASSERTIONS
#include "DebugCSPU.h"
#define DBG_LEFT_TO_READ 0
#define DBG_READ_DATA 0
#define DBG_RESET_READER 0

EEPROM_Bank::EEPROM_Bank(
  const unsigned int theMaintenancePeriodInSec) : EEPROM_BankWriter(theMaintenancePeriodInSec) {
  readerCurrentChip=0;
  readerCurrentAddress=0;
}

bool EEPROM_Bank::init(const EEPROM_Key theKey, const HardwareScanner &hw, const byte recordSize) {

  bool result = EEPROM_BankWriter::init(theKey, hw, recordSize);
  if (result) {
    readerCurrentChip = 0;
    readerCurrentAddress = sizeof(EEPROM_Header);
  }
  return result;
}


unsigned long EEPROM_Bank::leftToRead() const {
  unsigned long size;

  //first chip to read
  if (getHeader().firstFreeChip == readerCurrentChip) {
    size = getHeader().firstFreeByte - readerCurrentAddress;
    DPRINTS(DBG_LEFT_TO_READ, "readerCurrentAddress= ");
    DPRINTLN(DBG_LEFT_TO_READ, readerCurrentAddress);
    DPRINTS(DBG_LEFT_TO_READ, "firstFreeByte= ");
    DPRINTLN(DBG_LEFT_TO_READ, getHeader().firstFreeByte);
    DPRINTS(DBG_LEFT_TO_READ, "firstChipSize= ");
    DPRINTLN(DBG_LEFT_TO_READ, size);
  }
  else {
    size = ((unsigned long)getHeader().chipLastAddress - readerCurrentAddress + 1);
    DPRINTS(DBG_LEFT_TO_READ, "B. size= ");
    DPRINTLN(DBG_LEFT_TO_READ, size);
    for (int i = readerCurrentChip + 1; i < getHeader().firstFreeChip; i++) {
      size += ( (unsigned long) getHeader().chipLastAddress + 1L);
      DPRINTS(DBG_LEFT_TO_READ, " C. size= ");
      DPRINTLN(DBG_LEFT_TO_READ, size);
    }
    size += getHeader().firstFreeByte;
    DPRINTS(DBG_LEFT_TO_READ, " -D. size= ");
    DPRINTLN(DBG_LEFT_TO_READ, size);
  }
  return size;
}

void EEPROM_Bank::resetReader() {
  readerCurrentChip = 0;
  readerCurrentAddress = sizeof(EEPROM_Header);
  DPRINTS(DBG_RESET_READER, "RESETreaderCurrentAddress= ");
  DPRINTLN(DBG_RESET_READER, readerCurrentAddress);
  DPRINTS(DBG_RESET_READER, "RESETEEPROM_Header= ");
  DPRINTLN(DBG_RESET_READER, sizeof(EEPROM_Header));
}

byte EEPROM_Bank::readData(byte* data, const byte dataSize)
{
  byte toRead = dataSize;
  byte read;
  DPRINTS(DBG_READ_DATA, "leftToRead = ");
  DPRINTLN(DBG_READ_DATA, leftToRead());
  DPRINTS(DBG_READ_DATA, "dataSize = ");
  DPRINTLN(DBG_READ_DATA, dataSize);
  DPRINTS(DBG_READ_DATA, "readerCurrentAddress= ");
  DPRINTLN(DBG_READ_DATA, readerCurrentAddress);
  DPRINTS(DBG_READ_DATA, "readerCurrentChip= ");
  DPRINTLN(DBG_READ_DATA, readerCurrentChip);
  DPRINTS(DBG_READ_DATA, "firstFreeByte= ");
  DPRINTLN(DBG_READ_DATA, getHeader().firstFreeByte);

  // Handle the case of 0 bytes to read: just do nothing...
  if (toRead == 0) {
    return 0; // read 0 bytes.
  }
  
  if (leftToRead() < toRead) {
    toRead = leftToRead();
  }
  if (getHeader().firstFreeChip == readerCurrentChip)
  {
    DPRINTLN(DBG_READ_DATA, "firstFreeChip==readerCurrentChip");
    if (readerCurrentAddress - 1 <= getHeader().chipLastAddress - toRead)
    {
      DPRINTLN(DBG_READ_DATA, "readFromCurrentChip only");
      read = readFromEEPROM(readerCurrentChip, readerCurrentAddress, data, toRead);
      DPRINTS(DBG_READ_DATA, "read = ");
      DPRINTLN(DBG_READ_DATA, read);
      //next time, read after
      readerCurrentAddress = readerCurrentAddress + read;
      DPRINTS(DBG_READ_DATA, "read = ");
      DPRINTLN(DBG_READ_DATA, read);
      return read;

    }
    // This place we should NEVER reach.
    DASSERT(false);
  }

  DPRINTSLN(DBG_READ_DATA, "firstFreeChip!=readerCurrentChip");
  // BUG  previous version of calculation of the maximum size we can read from this chip was: 
  //      byte size1 = ((unsigned long) getHeader().chipLastAddress - readerCurrentAddress + 1);
  // The line above can overflow!! Size1 can be up to chipLastAddress+1 i.e more than an
  // unsigned long (16 bytes) can hold.
  // The only question here is "Is the data left in the current chip < dataSize ?
  // (getHeader().chipLastAddress - readerCurrentAddress is 1 less than the number of bytes left, and
  // can be chipLastAddress. Adding 1 would overflow, so compare to dataSize-1.
  unsigned long availableInThisChip=getHeader().chipLastAddress - readerCurrentAddress;
  bool exhaustingCurrentChip= (availableInThisChip == (byte) (dataSize - 1));
  // Casting to (dataSize -1) to (byte) above is safe: we know that dataSize >0. 
  if (availableInThisChip >= (byte) (dataSize - 1)) {
	  availableInThisChip = dataSize;
  }
  else availableInThisChip++; // We can safely add the 1 byte to have the exact count.
  DPRINTS(DBG_READ_DATA, "availableInThisChip=");
  DPRINT(DBG_READ_DATA, availableInThisChip);
  DPRINTS(DBG_READ_DATA, ", exhaustingCurrentChip:");
  DPRINTLN(DBG_READ_DATA, exhaustingCurrentChip);

  // This is the maximum size we can possibly read in this chip.
  // If >= dataSize, we read just dataSize, and we're done.
  // If < dataSize, we'll have to read a complement in the next chip.
  if (availableInThisChip >= dataSize)
  {
    read = readFromEEPROM(readerCurrentChip, readerCurrentAddress, data, dataSize);
    // BUG: The next line was readerCurrentAddress += read; which overflows when
    //      readerCurrentAddress = chipSize - dataSize.
    if (exhaustingCurrentChip && (read==dataSize)) {
    	    DPRINTSLN(DBG_READ_DATA, "exhaustingCurrentChip");
    		readerCurrentChip++;
    		readerCurrentAddress=0;
    } else {
    		readerCurrentAddress += read;
    }
    return read;
  }
  else
  {
    readFromEEPROM(readerCurrentChip, readerCurrentAddress, data, availableInThisChip);
    byte size2 = (dataSize - availableInThisChip);
    readerCurrentChip++;
    readFromEEPROM( readerCurrentChip, 0, &data[availableInThisChip], size2);
    readerCurrentAddress = size2;
    return dataSize;
  }
}


bool EEPROM_Bank::readerAtEnd() {
  return (leftToRead() == 0);
}

bool EEPROM_Bank::readOneRecord(byte* data, const byte dataSize) {
  if (dataSize == getHeader().recordSize) {
    byte byteRead = readData(data, dataSize);
    return (byteRead == dataSize);
  }
  else {
	  return false;
  }
}

unsigned long EEPROM_Bank::recordsLeftToRead() const {
  return (leftToRead() / getHeader().recordSize);
}
