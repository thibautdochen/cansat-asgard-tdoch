/*
 * LED_Type.h
 *
 *  A couple of types used in CansatProjects to manage LEDs.
 */

#pragma once

/** @ingroup CSPU_CansatConfig
 *  The various roles allocated to board LEDs */
enum class LED_Type {
	Init, 			/**< Switched on during initialization 					*/
	Storage, 		/**< Switched on during storage 						*/
	Transmission, 	/**< Switched on during RF transmission 				*/
	Acquisition,	/**< Switched on during Acquisition of sensor data 		*/
	Heartbeat, 		/**< Blinks continuously 								*/
	Campaign, 		/**< Switched on when measurement campaign is started 	*/
	UsingEEPROM 	/**< Can be used to denote the usage of EEPROM 		    */
};

/** @ingroup CSPU_CansatConfig
 *  The various states for a LED */
enum class LED_State {
	On, Off
};
