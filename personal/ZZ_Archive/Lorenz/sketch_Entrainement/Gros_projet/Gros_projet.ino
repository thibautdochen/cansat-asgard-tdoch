
const int LampeChaud = 13;
const int LampeFroide = 11;
const int CapteurTemperature = A0;
const int Ventilateur = 3;
const int EcritureTemperature = 1000;
const int TempsVentilateur = 10000;
const int AllumageCapteurTemperature = 6;

#define DEBUG_CSPU
#include "cansat_debug.h"
#define DBG_TEMPERATURE 0

void setup() {
  pinMode(CapteurTemperature, INPUT);
  Serial.begin(9600);
  pinMode(LampeChaud, OUTPUT);
  pinMode(LampeFroide, OUTPUT);
  pinMode(Ventilateur, OUTPUT);
  pinMode(AllumageCapteurTemperature, OUTPUT);
}

void loop() {
  digitalWrite(AllumageCapteurTemperature, HIGH);
  int val = analogRead(CapteurTemperature);
  //Serial.println(val);
  float tension = (val * 5.0) / 1023.0;
  //Serial.println(tension);
  float temperature = tension * 100.0;
  DPRINTLN(DBG_TEMPERATURE,temperature);

  if (temperature > 23) {
    digitalWrite(Ventilateur, HIGH);
    digitalWrite(LampeChaud, HIGH);
    digitalWrite(LampeFroide, LOW);
    delay(EcritureTemperature);

  }
  if (temperature < 23) {
    digitalWrite(LampeFroide, HIGH);
    digitalWrite(Ventilateur, LOW);
    digitalWrite(LampeChaud, LOW);
    delay(EcritureTemperature);
  }
  DPRINTSLN(1,"MSG");
  if (Ventilateur, HIGH) {
    Serial.println("Ventilateur Checked");
  }
  if (LampeFroide, HIGH) {
    Serial.println("LampeFroide Checked");
  }
  if (LampeChaud, HIGH) {
    Serial.println("LampeChaud Checked");
  }
}


