#include "SOSLed.h" //Include the headers that contain our objects

//Definition of the constructor and the method
SOSLed::SOSLed(byte thePinNumber, uint32_t theDurationBwSos) {
  pinNumber = thePinNumber;
  durationBwSos = theDurationBwSos;
  pinMode(pinNumber, OUTPUT);
  ts = millis();
}


//The SOS code
bool SOSLed::clicS() {    //This function prints the letter S
  if (millis() - ts >= durationPoint) {
    ts = millis();
    digitalWrite(pinNumber, !digitalRead(pinNumber));
    sosCounter++;
    if (sosCounter > 0 && sosCounter < 6) {
      return false;
    }
    else if (sosCounter == 6) {
      sosCounter = 0;
      ts = millis();
      return true;
    }
  }
  else {
    return false;
  }
}
bool SOSLed::clicO() {    //This function prints the letter O
  if (millis() - ts >= durationPoint && sosCounter % 2 == 0  && sosCounter < 6) {
    ts = millis();
    digitalWrite(pinNumber, !digitalRead(pinNumber));
    sosCounter++;
    return false;
  }
  else if (millis() - ts >= durationUnderscore && sosCounter % 2 != 0 && sosCounter < 6) {
    ts = millis();
    digitalWrite(pinNumber, !digitalRead(pinNumber));
    sosCounter++;
    return false;
  }
  else if (sosCounter == 6) {
    sosCounter = 0;
    ts = millis();
    return true;
  }
  else {
    return false;
  }
}
bool SOSLed::pause() {    //This function does the pause between SOS
  if (millis() - ts >= durationBwSos) {
    ts = millis();
    return true;
  }
  else {
    return false;
  }
}
void SOSLed::run() {  //this method is used to do a SOS on the led
  bool stateSwitch; //This variable is used to switch between state
  switch (sosState) {
    case state_t::first_s :
      stateSwitch = clicS();
      if (stateSwitch) {
        sosState = state_t::middle_o;
      }
      break;
    case state_t::middle_o :
      stateSwitch = clicO();
      if (stateSwitch) {
        sosState = state_t::second_s;
      }
      break;
    case state_t::second_s :
      stateSwitch = clicS();
      if (stateSwitch) {
        sosState = state_t::wait;
      }
      break;
    case state_t::wait:
      stateSwitch = pause();
      if (stateSwitch) {
        sosState = state_t::first_s;
      }
      break;
  }
}
