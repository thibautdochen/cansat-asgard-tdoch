#include "Arduino.h"
class SOS {
  public:
    bool begin(uint8_t PinNbr);
    void flash();

  private:
    void flashOnce(uint16_t duration);
    void S();
    void O();

    uint8_t LED_PinNbr;

    const uint16_t DurationS = 100;
    const uint16_t DurationO = 500;

};
