/*  Test program compatiblity of the RGB_DotStar LED on the
    ItsyBitsy M4 and the timers based on TIMER_TC_3 on SAMD21/SAMD51

    Turning off LED when timer is running is OK, even with period of 2 msec. 
*/
#ifndef ARDUINO_ITSYBITSY_M4
#error "Test currently only supports ItsyBitsy M4"
#endif

#define CSPU_DEBUG
#include "CansatConfig.h"
#include "DebugCSPU.h"
#include "Adafruit_DotStar.h"
#include "CSPU_Test.h"
#include "Serial2.h"
#include "SAMD_InterruptTimer.h"

constexpr bool FastInterruptTest=true; // set to true to run the time every couple of msec. 
constexpr uint16_t TimerPeriod = (FastInterruptTest ? 2 : 3000);

uint32_t isrCounter=0;

void coucou() {
  isrCounter++;
  if (!FastInterruptTest)  Serial << "Coucou at " << millis()  << ENDL;
}


void setup() {
  DINIT(115200);
  Serial2.begin(115200);


  Serial << "Creating SAMD timer with period=" << TimerPeriod << " msec" << ENDL;
  SAMD_InterruptTimer theTimer;
  theTimer.start(TimerPeriod, coucou);
  CSPU_Test::pressAnyKey();
  Serial << "Interrupt handler called " << isrCounter << " times..." << ENDL; 

  Serial << "Turning off RGB LED..." << ENDL;
  Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6);
  Serial << "a" << ENDL;
  theDotStar.begin();
  Serial << "b" << ENDL;
  theDotStar.show(); // Shutdown
  Serial << "Dotstar now shut down" << ENDL;
  Serial << "Interrupt handler called " << isrCounter << " times..." << ENDL; 


  CSPU_Test::pressAnyKey();
  theTimer.clear();
  Serial << "Interrupt handler called " << isrCounter << " times..." << ENDL; 
  Serial << "End of job " << ENDL;
  exit(0);
}

void loop() {
  // put your main code here, to run repeatedly:

}
