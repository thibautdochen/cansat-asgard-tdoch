#include "BlinkingLED.h"

BlinkingLED led1(12, 2000);
BlinkingLED led2(11,300);

void setup() {
  // put your setup code here, to run once:
}

void loop() {
  // put your main code here, to run repeatedly:
  led1.run();
  led2.run();
  
}
