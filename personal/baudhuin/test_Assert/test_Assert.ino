/*
    Test use of assert functions on Arduino
    Ref: https://gist.github.com/jlesech/3089916
*/

#include "config.h"

void setup() {
  // put your setup code here, to run once:
  DINIT(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  DPRINTSLN(1,"Entering loop");
  DDELAY(SLOW_DOWN, 1000);
  assert(2>1);
  DPRINTSLN(1,"Assert 1 OK");
  DDELAY(SLOW_DOWN,1000);
  assert(1>2);
  DPRINTSLN(1, "Exiting loop");
}
