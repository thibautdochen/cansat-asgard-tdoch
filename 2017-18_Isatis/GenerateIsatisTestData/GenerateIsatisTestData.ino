/*
    Sketch to generate realistic test data and store it on SD card.
    Intended to run on the Isatis stack.

    Still TO DO: add thermal inertia.
*/

#include "StorageManager.h"
#include "IsatisHW_Scanner.h"
#include "IsatisConfig.h"
#include <SoftwareSerial.h>
#include "IsatisStorageManager.h"
#include "GPY_Calculator.h"
#include "StringStream.h"
#include "elapsedMillis.h"

#include "Settings.h"
#include "CalculationFunctions.h"

IsatisStorageManager storageMgr(IsatisAcquisitionPeriod, IsatisCampainDuration);
IsatisDataRecord record;
IsatisHW_Scanner hw;

void completeRecord()
{
    float relativeAltitude= record.BMP_Altitude-BaseAltitude;
    record.BMP_Pressure= getPressureFromAltitude(relativeAltitude);
    record.BMP_Temperature=getTemperatureFromAltitude(relativeAltitude);
    record.GPY_OutputV=getGPY_OutputV(relativeAltitude);
    record.GPY_Quality=getGPY_Quality();
    record.GPY_DustDensity=GPY_Calculator::getDustDensity(record.GPY_OutputV, record.GPY_Quality);
    record.GPY_Voc=GPY_Calculator::Voc;
    record.GPY_AQI=GPY_Calculator::getAQI(record.GPY_DustDensity);
}

void setup() {
  Serial.begin(serialBaudRate); // The baud rate to communicate on USB port
  while (!Serial) ;
  pinMode(LED_BUILTIN, OUTPUT);

  hw.IsatisInit();
  hw.printFullDiagnostic(Serial);
  Serial << F("Generating test data for ISATIS...") << ENDL;
  Serial << F("Be sure to archive file Settings.h to keep information about generated data.") << ENDL;
  
  String CSV_Header;
  CSV_Header.reserve(50);
  StringStream sstr(CSV_Header);
  record.printCSV_Header(sstr);
  storageMgr.init(&hw, "SIMU", SD_CardChipSelect, SD_RequiredFreeMegs, EEPROM_KeyValue, CSV_Header);

  if (!storageMgr.SD_Operational()) {
    Serial << F("No SD card detected: data will not be stored") << ENDL;
  }
  record.clear();
  randomSeed(analogRead(unusedAnalogInPinNumber));
  
  // TODO: writeDiagnostic data ? Add generation data & time in file
  //       add generation parameters in file
  // Conclusion of Init
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  Serial << F("---------------------------------") << ENDL << ENDL;
  elapsedMillis startOfJob;
  
  if (PrintToSerial)  {
    record.printCSV_Header(Serial);
    Serial << ENDL;
  }
  
  unsigned long time;
  
  // Generate preflight data
  Serial << ENDL << F("*** PREFLIGHT DATA (") << (float) (DurationUntilLaunch-LoggingStartTime)/1000.0 << F(" sec) *****") << ENDL;
  for (time=LoggingStartTime; time <= DurationUntilLaunch;time+=IsatisAcquisitionPeriod)
  {
    record.startTimestamp=addTimeJitter(time);
    record.endTimestamp=getMeasurementDuration(time);
    record.BMP_Altitude=addAltitudeJitter(BaseAltitude);
    completeRecord();
    storageMgr.storeIsatisRecord(record,false);
    if (PrintToSerial) {
      record.printCSV(Serial);
      Serial << ENDL;
    }
  }
  
  // Generate ascent data.
  Serial << ENDL << F("*** ASCENT DATA (") << ((float) DurationOfAscent)/1000.0 << F(" sec from ") 
         << BaseAltitude << F(" to ") 
         << MaxAltitude << F(" m = ") 
         << AscentSpeed << F(" m/s) *****") << ENDL;
  for (; time< DurationUntilDescent;time+=IsatisAcquisitionPeriod)
  {
    record.startTimestamp=addTimeJitter(time);
    record.endTimestamp=getMeasurementDuration(time);
    record.BMP_Altitude=addAltitudeJitter(BaseAltitude+((time-DurationUntilLaunch)*AscentSpeed));
    completeRecord();
    storageMgr.storeIsatisRecord(record,false);
    if (PrintToSerial) {
      record.printCSV(Serial);
      Serial << ENDL;
    }
  }

  //Generate descent data
  Serial << ENDL <<F("*** DESCENT DATA (") << (float) DurationOfDescent/1000.0 << F(" sec from ") 
         << MaxAltitude << F(" to ") << BaseAltitude << F(" m = ") 
         << DescentSpeed << F(" m/s) *****") << ENDL;
  for (; time< DurationUntilLanding;time+=IsatisAcquisitionPeriod)
  {
    record.startTimestamp=addTimeJitter(time);
    record.endTimestamp=getMeasurementDuration(time);  
    record.BMP_Altitude=addAltitudeJitter(MaxAltitude-( (float) (time-DurationUntilDescent)*DescentSpeed/1000.0));
    completeRecord();
    storageMgr.storeIsatisRecord(record,false);
    if (PrintToSerial) {
      record.printCSV(Serial);
      Serial << ENDL;
    }
  }

  //Generate after-landing data
  Serial << ENDL << F("*** AFTER LANDING DATA (") << (float) DurationAfterLanding/1000.0 << F(" sec) *****") << ENDL;
  for (; time< DurationUntilPowerDown;time+=IsatisAcquisitionPeriod)
  {
    record.startTimestamp=addTimeJitter(time);
    record.endTimestamp=getMeasurementDuration(time);  
    record.BMP_Altitude=addAltitudeJitter(BaseAltitude);
    
    completeRecord();
    storageMgr.storeIsatisRecord(record,false);
    if (PrintToSerial) {
      record.printCSV(Serial);
      Serial << ENDL;
    }
  }

  Serial << ENDL <<  F("End of Job (duration: ") << (float) startOfJob/1000.0 << F(" sec).") << ENDL;
}

void loop() {

}

