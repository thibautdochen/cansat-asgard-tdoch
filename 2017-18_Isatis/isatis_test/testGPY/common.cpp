
#include "common.h"
#include "GPY_Calculator.h"
#include "output.h"

// ----- Shared globals definitions ----------------
double minOutputV, maxOutputV;
double minDensity, maxDensity;
double totalOutputV;
double minOkOutputV, maxOkOutputV;
double minOkDensity, maxOkDensity;
double totalOkOutputV;
double totalOkDensity;
unsigned long minReadingDuration, maxReadingDuration;
long numOK_Readings;

void resetGlobals()
{
  minOutputV = minOkOutputV = 1000.0;
  maxOutputV = maxOkOutputV = 0.0;
  minDensity = minOkDensity = 1000000.0;
  maxDensity = maxOkDensity =  0.0;
  totalOutputV = totalOkOutputV = totalOkDensity= 0.0;
  minReadingDuration = 9999999;
  maxReadingDuration = 0.0;
  numOK_Readings = 0;

}
void performOneReading( unsigned int readingId,
                        const byte nbrOfSamplesToAverage,
                        const double maxDelta,
                        const bool repeatBadQualityReadings,
                        const bool printResults) {
  unsigned long start, stop;
  start = millis();
  bool quality;
  double outputV = GPY::getOutputV_Average(   GPY_dataOutPinNbr, GPY_LED_VccPinNbr, nbrOfSamplesToAverage, maxDelta,
                   repeatBadQualityReadings, quality);
  stop = millis();
  double readingDuration = stop - start;
  double dustDensity = GPY_Calculator::getDustDensity(outputV, quality);

  if (outputV < minOutputV) minOutputV = outputV;
  if (outputV > maxOutputV) maxOutputV = outputV;
  if (readingDuration < minReadingDuration) minReadingDuration = readingDuration;
  if (readingDuration > maxReadingDuration) maxReadingDuration = readingDuration;
  totalOutputV += outputV;
  if (dustDensity < minDensity) minDensity = dustDensity;
  if (dustDensity > maxDensity) maxDensity = dustDensity;
  if (quality) {
    numOK_Readings++;
    if (outputV < minOkOutputV) minOkOutputV = outputV;
    if (outputV > maxOkOutputV) maxOkOutputV = outputV;
    totalOkOutputV += outputV;
    if (dustDensity < minOkDensity) minOkDensity = dustDensity;
    if (dustDensity > maxOkDensity) maxOkDensity = dustDensity;
    totalOkDensity+=dustDensity;

  }
  if (printResults) {
    printData(readingId, outputV, dustDensity, readingDuration, quality, Serial);
 //   printData(readingId, outputV, dustDensity, readingDuration, quality, Serial1);
  }
  long delayUntilEndOfPeriod = IsatisAcquisitionPeriod - (millis() - start);
  if (delayUntilEndOfPeriod > 0)  {
    delay(delayUntilEndOfPeriod);
  }
}

byte selectFrom(  const __FlashStringHelper* question,
                  const __FlashStringHelper* answer[],
                  const byte numAnswers) {
  __FlashStringHelper* indent = F("  ");
  bool ok = false;
  byte choice;
  Serial << question << F(":") << ENDL;
  for (byte i = 0; i < numAnswers; i++) {
    Serial << indent << i + 1 << F(". ") << answer[i] << ENDL;
  }
  while (!ok) {
    while (Serial.available() > 0) Serial.read(); // Discard any residual character.
    Serial << F("Your choice (1..") << numAnswers << F(") ? ");
    while (!Serial.available());
    char b = Serial.read();
    Serial << b << ENDL;
    choice = b - '0';
    if ((choice > 0) && (choice <= numAnswers)) {
      ok = true;
    }
    else  {
      Serial << F("  Invalid value '") << b << F("'") << ENDL;
      ok = false;
    }
  }
  return choice;
}

bool selectYN(  const __FlashStringHelper* question) {
  bool ok = false;
  bool choice;

  while (!ok) {
    while (Serial.available()) Serial.read(); // Discard any residual character.
    Serial << question << F(" (Y/N): ");
    while (!Serial.available());
    char b = Serial.read();
    Serial << b << ENDL;
    switch (b) {
      case 'Y':
      case 'y':
        choice = true;
        ok = true;
        break;
      case 'N':
      case 'n':
        choice = false;
        ok = true;
        break;
      default:
        Serial << F("  Invalid choice '") << b << F("'") << ENDL;
        ok = false;
    }
  }
  return choice;
}

