
/*
   This function defines a value for GPY_MaxDeltaInAverageSet for a particular value of
   GPY_NbrOfSamples in order to have targetGoodQualityPercentage % of good quality readings.
*/

#include "optimize.h"
#include "common.h"
#include "GPY.h"
#include "output.h"

const byte targetGoodQualityPercentage = 75;
const byte targetGoodQualityMargin = 5;  // optimization with be perform for targetGoodQualityPercentage + targetGoodQualityMargin
const byte minNbrOfSamplesToAverage = 6;
const byte maxNbrOfSamplesToAverage = 8;
const double maxDeltaMinimum = 0.05;
const double maxDelatMaximum = 3.0;
const double deltaAccuracy = 0.05;


float getOK_Ratio(const byte nbrOfSamplesToAverage, const double maxDelta, const bool printResults = true) {
  resetGlobals();
  for (byte i = 1 ; i <= numReadings; i++) {
    performOneReading(i, nbrOfSamplesToAverage, maxDelta, false, false);
  }
  float ratio = ((float) numOK_Readings) / numReadings;
  if (printResults) {
    Serial << F("   ") << nbrOfSamplesToAverage << F(" sample(s), maxDeltaV=") << maxDelta << F(": ")
           << ratio * 100 << F("% ok") << F(", deltaV accross readings= ") << maxOutputV - minOutputV << ENDL;
  }
  return ratio;
}

float optimizeSettingsFor(const byte nbrOfSamplesToAverage)
{

  if (nbrOfSamplesToAverage == 1) {
    // Just output result, for reference: With one sample, all tests are successful (and meaningless). 
    getOK_Ratio(nbrOfSamplesToAverage, 1, false);
    printResults(Serial);
    return 1;
  }
  
  double newRatio;
  double lowerBound = maxDeltaMinimum;
  double upperBound = maxDelatMaximum;
  float target = ((float) (targetGoodQualityPercentage+ targetGoodQualityMargin)) / 100.0;
  float lowerBoundRatio = getOK_Ratio(nbrOfSamplesToAverage, lowerBound);
  float upperBoundRatio;
  if (lowerBoundRatio >= target) {
    upperBound = lowerBound;
    upperBoundRatio = lowerBoundRatio;
  } else {
    upperBoundRatio = getOK_Ratio(nbrOfSamplesToAverage, upperBound);
  }

  // Bissection to find optimum
  while ((upperBound - lowerBound) > deltaAccuracy)
  {
    double middle = (lowerBound + upperBound) / 2;
    newRatio = getOK_Ratio(nbrOfSamplesToAverage, middle);
    if (newRatio >= target) upperBound = middle;
    else {
      lowerBound = middle;
      if (newRatio >= target) upperBound = lowerBound;
    }
  }
  double result = (upperBound + lowerBound) / 2.0;
  Serial << F("Result for ") << nbrOfSamplesToAverage << F(" sample(s): ") << result
         << F("V. Checking stability by repeating test for this value:");
  bool isStable = true;
  float percent;
  for (int i = 0 ; i < 5; i++)
  {
    newRatio = getOK_Ratio(nbrOfSamplesToAverage, result, false);
    percent = newRatio * 100;
    Serial << F(" ") << percent << F("%");
    if (percent < targetGoodQualityPercentage) {
      isStable = false;
      break;
    }
  }
  if (isStable) Serial << F(" CONFIRMED") << ENDL;
  else {
    Serial << F(" UNSTABLE") << ENDL;
    result = upperBound;
    Serial << F(" Using ") << result << F("V and checking: ");
    isStable=true;
    for (int i = 0 ; i < 5; i++)
    {
      newRatio = getOK_Ratio(nbrOfSamplesToAverage, result, false);
      percent = newRatio * 100;
      Serial << F(" ") << percent << F("%");
      if (percent < targetGoodQualityPercentage) {
        isStable = false;
        break;
      }
    }
    if (isStable) Serial << F(" CONFIRMED") << ENDL;
    else {
      Serial << F(" UNSTABLE :-( ") << ENDL;
    }
  }
  if (isStable) printResults(Serial);
  return result;
}

void optimizeSettings() {
  GPY::setFanOn(useFan);
  Serial << F("Optimizing number of samples/reading, and maximum allowed dispersion to have >") << targetGoodQualityPercentage << F("% good quality readings...") << ENDL;
  Serial << F("  Adding a ") << targetGoodQualityMargin << "% security margin" << ENDL; 
  Serial << F("  Using ") <<  minNbrOfSamplesToAverage << F(" to ") << maxNbrOfSamplesToAverage << F(" samples, and ") << numReadings
         << F(" readings/test") << ENDL;
  Serial << F("  Reading a group of samples at most once / ") << IsatisAcquisitionPeriod  << F(" msec.") << ENDL;
  Serial << F("This test addresses the STABILITY, not the calibration") << ENDL;
  for (byte nbrOfSamplesToAverage = maxNbrOfSamplesToAverage ; nbrOfSamplesToAverage <= maxNbrOfSamplesToAverage; nbrOfSamplesToAverage++)
  {
    optimizeSettingsFor(nbrOfSamplesToAverage);
  }
  GPY::setFanOn(false);
}
