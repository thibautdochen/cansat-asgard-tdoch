/**
  Utility sktech to display the XBee nodes discovered in the network.
  Test with Feather M0 board (should work with any other SAMD board...)

  Wiring: µC to XBee module
   3.3V to VCC
   RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
   TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
   GND  to GND
*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#  error "This program only works on SAMD boards"
#endif

#include "CansatConfig.h"
#include "XBeeClient.h"
#include "XBee.h"

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

HardwareSerial &RF = Serial1;
XBeeClient xbc(0x1234, 0x5678); // adresses are irrelevant here

void setup() {
  DINIT(115200);
  Serial << "***** XBee Network discovery sketch *****" << ENDL;
  Serial << "Initialising Serials and communications..." << ENDL;
  RF.begin(115200);
  xbc.begin(RF); // no return value

  xbc.printConfigurationSummary(Serial);

  Serial << ENDL << "Initialisation over." << ENDL;
}

void loop() {
   xbc.printDiscoveredNodes();
   Serial << ENDL;
   delay(3000);
}
